package Client;

import Common.Car;
import Common.MethodsInterface;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;

import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

public class ViewController {

    @FXML
    private TextField carYear;

    @FXML
    private TextField carEngine;

    @FXML
    private TextField carPrice;

    @FXML
    private Button taxesButton;

    @FXML
    private Button sellButton;

    @FXML
    private TextField resultField;

    @FXML
    void computeSellPrice(ActionEvent event) throws RemoteException, NotBoundException {
        Registry registry = LocateRegistry.getRegistry("localhost",1099);
        MethodsInterface mi = (MethodsInterface) registry.lookup(MethodsInterface.class.getSimpleName());
        Car newCar = new Car(Integer.parseInt(carYear.getText()),Integer.parseInt(carEngine.getText()),Double.parseDouble(carPrice.getText()));
        Double result = mi.computeSellingPrice(newCar);
        resultField.setText(result.toString());
    }

    @FXML
    void computeTaxes(ActionEvent event) throws RemoteException, NotBoundException {
        Registry registry = LocateRegistry.getRegistry("localhost",1099);
        MethodsInterface mi = (MethodsInterface) registry.lookup(MethodsInterface.class.getSimpleName());
        Car newCar = new Car(Integer.parseInt(carYear.getText()),Integer.parseInt(carEngine.getText()),Double.parseDouble(carPrice.getText()));
        Double result = mi.computeTax(newCar);
        resultField.setText(result.toString());
    }
}
