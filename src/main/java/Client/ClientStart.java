package Client;

        import Common.Car;
        import Common.MethodsInterface;

        import java.rmi.NotBoundException;
        import java.rmi.RemoteException;
        import java.rmi.registry.LocateRegistry;
        import java.rmi.registry.Registry;

public class ClientStart {
    public static void main(String[] args) throws RemoteException, NotBoundException {
        Registry registry = LocateRegistry.getRegistry("localhost",1099);
        MethodsInterface mi = (MethodsInterface) registry.lookup(MethodsInterface.class.getSimpleName());
        Car car1 = new Car( 2014, 2000, 4000);
        System.out.println(mi.computeTax(car1));
        System.out.println(mi.computeSellingPrice(car1));
    }
}
