package Common;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface MethodsInterface extends Remote {

    double computeSellingPrice(Car c) throws RemoteException;
    double computeTax(Car c) throws RemoteException;
}
