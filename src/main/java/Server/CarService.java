package Server;

import Common.Car;
import Common.MethodsInterface;

import java.rmi.RemoteException;

public class CarService implements MethodsInterface {

    public double computeSellingPrice(Car c) throws RemoteException {
        if(c.getPrice() <= 0) {
            throw new IllegalArgumentException("Price must be positive");
        }
        if(2018-c.getYear()< 7) {
            return c.getPrice()-c.getPrice()/7 * (2018 - c.getYear());
        } else {
            return 0;
        }
    }

    public double computeTax(Car c) throws RemoteException {
        if (c.getEngineCapacity() <= 0) {
            throw new IllegalArgumentException("Engine capacity must be positive.");
        }
        int sum = 8;
        if(c.getEngineCapacity() > 1601) sum = 18;
        if(c.getEngineCapacity() > 2001) sum = 72;
        if(c.getEngineCapacity() > 2601) sum = 144;
        if(c.getEngineCapacity() > 3001) sum = 290;
        return c.getEngineCapacity() / 200.0 * sum;
    }
}
