package Server;

import Common.MethodsInterface;

import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;

public class ServerStart {
    public static void main(String[] args) {
        try {
            MethodsInterface mi = new CarService();
            MethodsInterface stub =
                    (MethodsInterface) UnicastRemoteObject.exportObject(mi, 0);
            Registry registry = LocateRegistry.createRegistry(1099);
            registry.rebind(MethodsInterface.class.getSimpleName(), stub);
            System.out.println("Server bound");
        } catch (Exception e) {
            System.err.println("Server exception:");
            e.printStackTrace();
        }
    }
}
